#include <iostream>
#include <event2/event.h>
#include <signal.h>

using namespace std;

// sock文件描述符，which事件类型，arg参数
void Ctrl_C(int sock, short which, void* arg)
{
	cout << "Ctrl_C event.." << endl;
}

//test: pkill test_signal
void Kill(int sock, short which, void* arg)
{
	cout << "Kill event.." << endl;

	event *ev = (event*)arg;
	//如果是非待决，重新添加
	if (!evsignal_pending(ev, NULL))
	{
		event_del(ev);
		event_add(ev, NULL);
	}
}

#if 0

int main(int argc, char* argv[])
{
#ifdef _WIN32
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);
#else
	//忽略管道信号，发送数据给已关闭的socket。错误忽略掉，防止程序dump掉
	if (signed(SIGPIPE, SIG_IGN) == SIG_ERR)
		return 1;
#endif

	event_base *base = event_base_new();
	//evsignal_new隐藏的EV_SIGNAL|EV_PERSIST
	event* csig = evsignal_new(base, SIGINT, Ctrl_C, base); //nopending
	if (!csig)
	{
		cerr << "csig evsignal_new error" << endl;
		return -1;
	}
	if (event_add(csig, 0) != 0)
	{
		cerr << "csig event_add error" << endl;
		return -1;
	}
	
	//非持久，只进入一次
	event* ksig = event_new(base, SIGTERM, EV_SIGNAL, Kill, event_self_cbarg());
	if (!ksig)
	{
		cerr << "ksig event_new error" << endl;
		return -1;
	}
	if (event_add(ksig, 0) != 0)
	{
		cerr << "ksig event_add error" << endl;
		return -1;
	}

	event_base_dispatch(base);
	event_free(csig);
	event_free(ksig);
	event_base_free(base);
	return 0;
}

#endif

//makefile:
//test_signal:test_signal.cpp
//g++ $^ -o $@ -levent

//make: