

#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <event2/event.h>
#include <event2/listener.h>
#include <event2/bufferevent.h>
#include <event2/buffer.h>
#include <thread>
#include <errno.h>
#include <string.h>
#include <locale.h>

using namespace std;
#define PORT 5001

bufferevent_filter_result filter_in(evbuffer *src, evbuffer *dst, ev_ssize_t dst_limit,
									 bufferevent_flush_mode mode, void *ctx)
{
	cout << "filter_in" << endl;//不处理的话，一直在触发
	//读取并清理原数据
	char data[1024] = { 0 };
	int len = evbuffer_remove(src, data, sizeof(data)-1);

	//所有字母转成大写
	for (int i = 0; i < len; i++)
		data[i] = toupper(data[i]);

	evbuffer_add(src, data, len);
	return BEV_OK;
}

bufferevent_filter_result filter_out(evbuffer *src, evbuffer *dst, ev_ssize_t dst_limit,
	bufferevent_flush_mode mode, void *ctx)
{
	cout << "filter_out" << endl;

	//读取并清理原数据
	char data[1024] = { 0 };
	int len = evbuffer_remove(src, data, sizeof(data)-1);

	string str = "";
	str += "========\r\n";
	str += data;
	str += "========\r\n";

	evbuffer_add(src, str.c_str(), str.length());
	return BEV_OK;
}


void read_cb4(bufferevent* bev, void *arg)
{
	cout << "read_cb4" << endl;
	char data[1024] = { 0 };
	//读取数据
	int len = bufferevent_read(bev, data, sizeof(data)-1);
	cout << data << endl;

	//回复客户消息， 经过输出过滤器
	bufferevent_write(bev, data, len);
}

void write_cb4(bufferevent* bev, void *arg)
{
	cout << "write_cb4" << endl;
}

void event_cb4(bufferevent* bev,  short events, void *arg)
{
	cout << "event_cb4" << endl;
}

void Listen_cb4(evconnlistener *ev, evutil_socket_t s, sockaddr* sin,
	int slen, void *arg)
{
	event_base *base = (event_base *)arg;

	cout << "Listen_cb4:" << endl;//不处理的话，会一直触发对于windows和水平出法。
	
	//create
	bufferevent *bev = bufferevent_socket_new(base, s, BEV_OPT_CLOSE_ON_FREE);
	bufferevent *bev_fileter = bufferevent_filter_new(
		bev, 
		filter_in, 
		filter_out, 
		BEV_OPT_CLOSE_ON_FREE, //关闭filter同事 关闭bufferevent
		0, 0 //清理地回调函数和参数
		);

	//回调
	bufferevent_setcb(bev_fileter, read_cb4, write_cb4, event_cb4, 
		0);//回调函数参数

	bufferevent_enable(bev_fileter, EV_READ | EV_WRITE);
}

#if 0
int main(int argc, char* argv[])
{
#ifdef _WIN32
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);
#else
	//忽略管道信号，发送数据给已关闭的socket。错误忽略掉，防止程序dump掉
	if (signed(SIGPIPE, SIG_IGN) == SIG_ERR)
		return 1;
#endif

	event_base *base = event_base_new();

	sockaddr_in sin;
	memset(&sin, 0, sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_port =htons(PORT);

	evconnlistener* ev = evconnlistener_new_bind(base,
		Listen_cb4,
		base,  //回调参数
		LEV_OPT_REUSEABLE | LEV_OPT_CLOSE_ON_FREE,
		10,
		(sockaddr*)&sin, sizeof(sin)
	);

	event_base_dispatch(base);//运行循环，阻塞函数
	evconnlistener_free(ev);
	event_base_free(base);

#ifdef _WIN32
	WSACleanup();
#endif
	return 0;
}
#endif

//读linux系统登陆文件/var/log/auth.log

//makefile:
//test_buffer_filter:test_buffer_filter.cpp
//g++ $^ -o $@ -levent

//make:

//test:telnet 127.0.0.1 5001

