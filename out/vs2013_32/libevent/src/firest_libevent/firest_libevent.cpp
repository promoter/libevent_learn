#include <iostream>


#include "event2/event.h"
#include "event2/listener.h"
using namespace std;

#define PORT 5001

//接收连接回调参数
void ev_cb(struct evconnlistener *e, evutil_socket_t socket, struct sockaddr *a, int socklen, void *arg)
{
	cout << "new connect.." << endl;
}

#if 0

int main(int argc, char* argv[])
{
#ifdef _WIN32
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);
#else
	//忽略管道信号，发送数据给已关闭的socket。错误忽略掉，防止程序dump掉
	if (signed(SIGPIPE, SIG_IGN) == SIG_ERR)
		return 1;
#endif
	event_base* base = event_base_new();
	if (base) cout << "libevent new successed." << endl;
	//监听端口
	sockaddr_in addr;
	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(PORT);
	evconnlistener* listener = evconnlistener_new_bind(base, 
		ev_cb, //回调函数
		base,	//回调函数参数
		LEV_OPT_REUSEABLE | LEV_OPT_CLOSE_ON_FREE, //地址重用，listen关闭同时清理socket
		10, //连接的最大队列大小，对于listen函数
		(sockaddr *)&addr, sizeof(addr)	//绑定的地址和端口
		);
	
	if (base) event_base_dispatch(base); //事件分发处理

	if (base) evconnlistener_free(listener);
	if (base) event_base_free(base);
	//测试方法。使用telnet工具（在windows功能中安装）
	//telnet 127.0.0.1 5001

#ifdef _WIN32
		WSACleanup();
#endif
	return 0;
}

#endif