//test_signal_loop.cpp

#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <event2/event.h>
#include <signal.h>

using namespace std;

bool isExit = false;
// sock文件描述符，which事件类型，arg参数
void Ctrl_C2(int sock, short which, void* arg)
{
	cout << "Ctrl_C event.." << endl;
	event_base *base = (event_base *)arg;
	//执行完当前处理地事件函数就退出
	//event_base_loopbreak(base);

	//运行完所有地活动事件后退出，事件循环没有运行室也要等待运行一次后再退出
	timeval t = { 3, 0 };//至少运行3s后退出
	event_base_loopexit(base, &t);
}

//test: pkill test_signal
void Kill2(int sock, short which, void* arg)
{
	cout << "Kill event.." << endl;

	event *ev = (event*)arg;
	//如果是非待决，重新添加
	if (!evsignal_pending(ev, NULL))
	{
		event_del(ev);
		event_add(ev, NULL);
	}
}

#if 0

int main(int argc, char* argv[])
{
#ifdef _WIN32
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);
#else
	//忽略管道信号，发送数据给已关闭的socket。错误忽略掉，防止程序dump掉
	if (signed(SIGPIPE, SIG_IGN) == SIG_ERR)
		return 1;
#endif

	event_base *base = event_base_new();

	//evsignal_new隐藏的EV_SIGNAL|EV_PERSIST
	event* csig = evsignal_new(base, SIGINT, Ctrl_C2, base); //nopending
	if (!csig)
	{
		cerr << "csig evsignal_new error" << endl;
		return -1;
	}
	if (event_add(csig, 0) != 0)
	{
		cerr << "csig event_add error" << endl;
		return -1;
	}

	//非持久，只进入一次
	event* ksig = event_new(base, SIGTERM, EV_SIGNAL, Kill2, event_self_cbarg());
	if (!ksig)
	{
		cerr << "ksig event_new error" << endl;
		return -1;
	}
	if (event_add(ksig, 0) != 0)
	{
		cerr << "ksig event_add error" << endl;
		return -1;
	}


	//event_base_dispatch(base);
	//EVLOOP_ONCE 等待一个事件运行，直到没有活动事件就退出。
	//EVLOOP_NONBLOCK 有活动事件就处理，没有就返回0
	//while (!isExit){
	//	event_base_loop(base, EVLOOP_NONBLOCK);
	//}
	//EVLOOP_NO_EXIT_ON_EMPTY 没有注册事件也不返回，用于事件后期多线程添加
	event_base_loop(base, EVLOOP_NO_EXIT_ON_EMPTY);

	event_free(csig);
	event_free(ksig);
	event_base_free(base);
	return 0;
}

#endif

//makefile:
//test_signal_loop:test_signal_loop.cpp
//g++ $^ -o $@ -levent

//make:
