//test_buffer_client.cpp

#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <event2/event.h>
#include <event2/listener.h>
#include <event2/bufferevent.h>
#include <signal.h>
#include <string>
using namespace std;

static string recvstr = "";
static int recvCount = 0;
static int sendCount = 0;

//错误，超时，连接断开会进入
void event_cb2(bufferevent *be, short what, void *ctx)
{
	cout << "[e]" << flush;
	//读取超时时间发生后，数据读取停止
	if (what & BEV_EVENT_TIMEOUT && what & BEV_EVENT_READING)
	{
		////读取缓存中数据,如果不读取，则客户端发送地和服务端接收的大小不一致。
		char data[1024] = { 0 };
		int len = bufferevent_read(be, data, sizeof(data)-1);
		if (len > 0)
		{
			recvstr += data;
			recvCount += len;
		}
		cout << " read timeout " << flush;
		//bufferevent_enable(be, EV_READ);
		bufferevent_free(be);

		//cout << recvstr << endl;
		cout << "recv count " << recvCount << endl;
		cout << "sendCount count " << sendCount << endl;
	}
	else if (what & BEV_EVENT_ERROR)
	{
		cout << " error " << flush;
		bufferevent_free(be);
	}
	else
	{
		cout << " timeout others " << what << flush;//17 :如果客户端直接断开。
		bufferevent_free(be);
	}
}

void write_cb2(bufferevent *be, void *arg)
{
	cout << "[w]" << flush;
}

void read_cb2(bufferevent *be, void *arg)
{
	cout << "[r]" << flush;
	char data[1024] = { 0 };
	int len = bufferevent_read(be, data, sizeof(data) - 1);
	//cout << "[" << data << "]" << endl;
	cout << data << flush;
	if (len <= 0) return;
	recvstr += data;
	recvCount += len;

}

void client_event_cb(bufferevent *be, short what, void *ctx)
{
	cout << "[client e]" << flush;
	//读取超时时间发生后，数据读取停止
	if (what & BEV_EVENT_TIMEOUT && what & BEV_EVENT_READING)
	{
		cout << " client read timeout " << flush;
		//bufferevent_enable(be, EV_READ);
		bufferevent_free(be);
		return;
	}
	else if (what & BEV_EVENT_ERROR)
	{
		cout << " client error " << flush;
		bufferevent_free(be);
		return;
	}
	//服务端地关闭事件
	if (what & BEV_EVENT_EOF)
	{
		cout << "client BEV_EVENT_EOF" << endl;

		bufferevent_free(be);
		return;
	}
	if (what & BEV_EVENT_CONNECTED)
	{
		cout << "client BEV_EVENT_CONNECTED" << endl;
		//触发write
		bufferevent_trigger(be, EV_WRITE, 0);

	}
}
 
void client_write_cb(bufferevent *be, void *arg)
{
	cout << "[client w]" << flush;
	FILE* fp = (FILE*)arg;
	char data[1024] = { 0 };
	int len = fread(data, 1, sizeof(data)-1, fp);
	if (len <= 0)
	{
		fclose(fp);
		//bufferevent_free(be);//立刻清理，可能导致缓存数据没有发送结束
		bufferevent_disable(be, EV_WRITE);
		return;//读到结尾或者文件出错
	}
	sendCount += len;
	//写入buffer
	bufferevent_write(be, data, len);

}

void client_read_cb(bufferevent *be, void *arg)
{
	cout << "[client r]" << flush;
	//char data[1024] = { 0 };
	//int len = bufferevent_read(be, data, sizeof(data)-1);
	//cout << "[" << data << "]" << endl;
	//if (len <= 0) return;
	//if (strstr(data, "quit") != NULL)
	//{
	//	cout << "quit" << endl;
	//	bufferevent_free(be);
	//}
	//bufferevent_write(be, "ok", 3);
}

void Listen_cb3(evconnlistener *ev, evutil_socket_t s, sockaddr* sin, 
	int slen, void *arg)
{
	cout << "Listen_cb3" << endl;
	event_base *base = (event_base*)arg;
	//创建bufferevent上下文
	bufferevent *buffer = bufferevent_socket_new(base, s,
		BEV_OPT_CLOSE_ON_FREE);

	//设置水位
	//读取水位
	bufferevent_setwatermark(buffer, EV_READ, 
		5, //低水位 0为无限制， 默认0
		10);//高水位 0为无限制， 默认0
	//写低水位
	bufferevent_setwatermark(buffer, EV_WRITE,
		5, //低水位 0为无限制， 默认0 缓存数据低于5 写入回调被使用
		0);//高水位 0为无限制， 默认0
	//超时时间设置
	timeval t1 = { 0, 500000 };
	bufferevent_set_timeouts(buffer, 
		&t1, //读超时 
		0); //写超时

	//添加监控事件 设置回调函数
	bufferevent_enable(buffer, EV_READ | EV_WRITE);
	bufferevent_setcb(buffer, read_cb2, write_cb2, event_cb2, base);
}
#if 0 

int main(int argc, char* argv[])
{
#ifdef _WIN32
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);
#else
	//忽略管道信号，发送数据给已关闭的socket。错误忽略掉，防止程序dump掉
	if (signed(SIGPIPE, SIG_IGN) == SIG_ERR)
		return 1;
#endif

	event_base *base = event_base_new();

	//创建网络服务器
	sockaddr_in sin;
	memset(&sin, 0, sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_port = htons(5001);

	evconnlistener* ev = evconnlistener_new_bind(base,
		Listen_cb3, 
		base,  //回调参数
		LEV_OPT_REUSEABLE | LEV_OPT_CLOSE_ON_FREE,
		10,
		(sockaddr*)&sin, sizeof(sin)
		);

	{
		//调用客户端代码
		//-1内部创建socket
		bufferevent *bev = bufferevent_socket_new(base, -1, BEV_OPT_CLOSE_ON_FREE);
		sockaddr_in sin;
		memset(&sin, 0, sizeof(sin));
		sin.sin_family = AF_INET;
		sin.sin_port = htons(5001);
		evutil_inet_pton(AF_INET, "127.0.0.1", &sin.sin_addr.S_un.S_addr);

		FILE *fp = fopen("test_buffer_client.cpp", "rb");
		//设置回调函数及权限
		bufferevent_enable(bev, EV_READ | EV_WRITE);
		bufferevent_setcb(bev, client_read_cb, client_write_cb, client_event_cb, fp);
		int ret = bufferevent_socket_connect(bev, (sockaddr*)&sin, sizeof(sin));
		if (ret == 0)
		{
			cout << "connect .." << endl;
		}
	}
	event_base_dispatch(base);
	event_base_free(base);
	return 0;
}

#endif

//makefile:
//test_buffer_client:test_buffer_client.cpp
//g++ $^ -o $@ -levent

//make:

