#include <iostream>


#include "event2/event.h"
#include "event2/listener.h"
using namespace std;

#define PORT 5001


#if 1

int main(int argc, char* argv[])
{
#ifdef _WIN32
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);
#else
	//忽略管道信号，发送数据给已关闭的socket。错误忽略掉，防止程序dump掉
	if (signed(SIGPIPE, SIG_IGN) == SIG_ERR)
		return 1;
#endif
	event_base* base = event_base_new();

	void Server(event_base* base);//函数声明
	Server(base);

	void Client(event_base* base);
	Client(base);
	
	if (base) event_base_dispatch(base); //事件分发处理
	if (base) event_base_free(base);

#ifdef _WIN32
	WSACleanup();
#endif
	return 0;
}

#endif

//makefile:
//test_buffer_filter_zlib:test_buffer_filter_zlib.cpp
//g++ $^ -o $@ -levent

//make:

//test:telnet 127.0.0.1 5001
