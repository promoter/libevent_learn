#include <iostream>
#include <event2/event.h>
#include <thread>
using namespace std;

static timeval t1 = { 1, 0 };
void Timer1(int sock, short which, void* arg)
{
	cout << "[Timer1]" << flush;
	event *ev = (event*)arg;
	//如果是非待决，重新添加
	if (!evtimer_pending(ev, &t1))
	{
		event_del(ev);
		event_add(ev, &t1);
	}
}

void Timer2(int sock, short which, void* arg)
{
	cout << "[Timer2]" << flush;
	this_thread::sleep_for(std::chrono::milliseconds(3000));
	//this_thread::sleep_for(3000ms);
}

void Timer3(int sock, short which, void* arg)
{
	cout << "[Timer3]" << flush;
}

#if 0

int main(int argc, char* argv[])
{
#ifdef _WIN32
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);
#else
	//忽略管道信号，发送数据给已关闭的socket。错误忽略掉，防止程序dump掉
	if (signed(SIGPIPE, SIG_IGN) == SIG_ERR)
		return 1;
#endif

	event_base *base = event_base_new();
	//定时器
	cout << "test timer " << endl;
	//#define evtimer_new(b, cb, arg)		event_new((b), -1, 0, (cb), (arg))
	//定时器事件  非持久事件 只响应一次
	event* evl = evtimer_new(base, Timer1, event_self_cbarg());
	if (!evl)
	{
		cerr << "evl evtimer_new error" << endl;
		return -1;
	}
	if (evtimer_add(evl, &t1) != 0)
	{
		cerr << "evl evtimer_add error" << endl;
		return -1;
	}

	static timeval t2;
	t2.tv_sec = 1;
	t2.tv_usec = 200000;//
	event* ev2 = event_new(base, -1, EV_PERSIST, Timer2, 0);//持久
	event_add(ev2, &t2);//插入O(logN);

	event* ev3 = event_new(base, -1, EV_PERSIST, Timer3, 0);//持久
	//超时性能优化，默认使用二叉堆存储（完全二叉树），插入删除O(logN);
	//优化到双向列表，插入删除O(1);
	static timeval tv_in = { 3, 0 };
	const timeval *t3;
	t3 = event_base_init_common_timeout(base, &tv_in);
	event_add(ev3, t3); //插入O(1);

	event_base_dispatch(base);
	event_free(evl);
	event_free(ev2);
	event_free(ev3);
	event_base_free(base);
	return 0;
}


#endif

//makefile:
//test_timer:test_timer.cpp
//g++ $^ -o $@ -levent

//make: