
#include <iostream>
#include <event2/event.h>
#include <thread>

#ifndef _WIN32
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#else
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#endif

using namespace std;

#if 0 
/*
	读取linux用户登录文件日志并监听文件更新。 
*/
void Read_File(evutil_socket_t fd, short event, void* arg)
{
	char buf[1024] = { 0 };
	int len = read(fd, buf, sizeof(buf)-1);
	if (len > 0)
	{
		cout << buf << endl;
	}
	else
	{
		cout << ".." << flush;
		this_thread::sleep_for(std::chrono::milliseconds(500));
		//this_thread::sleep_for(500ms);
	}
}

int main(int argc, char* argv[])
{
#ifdef _WIN32
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);
#else
	//忽略管道信号，发送数据给已关闭的socket。错误忽略掉，防止程序dump掉
	if (signed(SIGPIPE, SIG_IGN) == SIG_ERR)
		return 1;
#endif
	event_config* conf = event_config_new();
	//设置支持文件描述符
	event_config_require_features(conf, EV_FEATURE_FDS);
	event_base *base = event_base_new_with_config(conf);
	event_config_free(conf);
	if (!base)
	{
		cerr << "event_base_new_with_config failed!" << endl;
		return -1;
	}
	int sock = open("/var/log/auth.log", O_RDONLY|O_NONOBLOCK, 0);
	if (sock <= 0)
	{
		cerr << "open /var/log/auth.log failed!" << endl;
		return -2;
	}
	//文件指针移动到结尾处
	fseek(sock, 0, SEEK_END);
	//监听文件文件数据
	event* fev = event_new(base, sock, EV_READ | EV_PERSIST, Read_File, 0);
	event_add(fev, NULL);

	event_base_dispatch(base);
	event_free(fev);
	event_base_free(base);
	return 0;
}

#endif
//读linux系统登陆文件/var/log/auth.log

//makefile:
//test_file:test_file.cpp
//g++ $^ -o $@ -levent

//make: