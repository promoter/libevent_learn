
#define _CRT_SECURE_NO_WARNINGS

#include <iostream>

#include "event2/event.h"
#include "event2/listener.h"
#include "event2/bufferevent.h"
#include "event2/buffer.h"
using namespace std;
#define PORT 5001

#define FILEPATH "001.txt"


bufferevent_filter_result out_filter(evbuffer* s, evbuffer* d,
	ev_ssize_t limit, bufferevent_flush_mode mode, void* arg)
{
	cout << "out_filter" << endl; // ??? 为啥一直触发
	char data[1024] = { 0 };
	int len = evbuffer_remove(s, data, sizeof(data));
	evbuffer_add(s, data, len);

	return BEV_OK;
}

void client_read_cb2(bufferevent* bev, void* arg)
{
	//002 接收服务端发送地ok回复
	char data[1024] = { 0 };
	int len = bufferevent_read(bev, data, sizeof(data)-1);
	if (strcmp(data, "OK") == 0)
	{
		cout << "client_read_cb2: " << data << endl;
		//开始发送文件 触发write回调
		bufferevent_trigger(bev, EV_WRITE, 0);
	}
	else
	{
		bufferevent_free(bev);
	}
	cout << "client_read_cb2 len: " << len << endl;
	

}

void client_write_cb2(bufferevent* bev, void* arg)
{
	cout << "client_write_cb2" << endl;
	FILE *fp = (FILE *)arg;
	if (!fp) return;
	//读取文件
	char data[1024] = { 0 };
	int len = fread(data, 1, sizeof(data), fp);
	if (len <= 0)
	{
		fclose(fp);
		bufferevent_free(bev);//立刻清理
		return;
	}
	//发送文件
	bufferevent_write(bev, data, len);
}

void client_event_cb2(bufferevent *be, short events, void* arg)
{
	cout << "client_event_cb " << events << endl;
	if (events & BEV_EVENT_CONNECTED)
	{
		cout << "BEV_EVENT_CONNECTED" << endl;
		//001 发送文件名
		bufferevent_write(be, FILEPATH, strlen(FILEPATH));

		//创建输出过滤
		bufferevent* bev_filter = bufferevent_filter_new(be,
			0, out_filter, BEV_OPT_CLOSE_ON_FREE, 0, 0);
		FILE *fp = fopen(FILEPATH, "rb");
		if (!fp)
		{
			cout << "open file " << FILEPATH << "failed" << endl;
			return;
		}
		//设置读取、写入和事件的回调
		bufferevent_setcb(bev_filter, client_read_cb2, client_write_cb2, client_event_cb2, fp);
		bufferevent_enable(bev_filter, EV_READ | EV_WRITE);
	}
}

void Client(event_base* base)
{
	cout << "begin client " << endl;
	//连接服务器
	sockaddr_in sin;
	memset(&sin, 0, sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_port = htons(PORT);
	evutil_inet_pton(AF_INET, "127.0.0.1", &sin.sin_addr.S_un.S_addr);
	bufferevent *bev = bufferevent_socket_new(base, -1, BEV_OPT_CLOSE_ON_FREE);

	//只绑定连接事件回调，用来确认连接成功
	bufferevent_enable(bev, EV_WRITE | EV_READ);
	bufferevent_setcb(bev, 0, 0, client_event_cb2, 0);
	bufferevent_socket_connect(bev, (sockaddr*)&sin, sizeof(sin));
	//接收回复确认OK



}