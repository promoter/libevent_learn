
#include <iostream>


#include "event2/event.h"
#include "event2/listener.h"
#include "event2/bufferevent.h"
#include "event2/buffer.h"
using namespace std;
#define PORT 5001

bufferevent_filter_result input_filter(evbuffer* s, evbuffer* d,
	ev_ssize_t limit, bufferevent_flush_mode mode, void* arg)
{
	//1.接收客户端发送地文件名称
	char data[1024] = { 0 };
	int len = evbuffer_remove(s, data, sizeof(data)-1);
	cout << "server recv  input_filter " << data << endl;

	evbuffer_add(s, data, len);
	return BEV_OK;
}

void server_read_cb(bufferevent* bev, void* arg)
{
	//2.回复ok
	bufferevent_write(bev, "OK", 2);
}

void server_event_cb(bufferevent* bev, short events, void* arg)
{

}

//接收连接回调参数
void listen_cb_server(struct evconnlistener *e, evutil_socket_t socket,
					struct sockaddr *a, int socklen, void *arg)
{
	cout << "listen_cb_server" << endl;
	event_base *base = (event_base*)arg;
	//1.创建bufferevent用来通信
	bufferevent *bev = bufferevent_socket_new(base, socket, BEV_OPT_CLOSE_ON_FREE);

	//2 添加输入过滤 并设置输入回调
	bufferevent *bev_filter = bufferevent_filter_new(bev,
		input_filter, 0,
		BEV_OPT_CLOSE_ON_FREE, //关闭filter同时关闭bufferevent
		0, 0 //清理回调及参数
		);
	//3 设置回调 读取，事件（处理连接断开） 
	bufferevent_setcb(bev_filter, server_read_cb, 0, server_event_cb, 0);
	bufferevent_enable(bev_filter, EV_READ | EV_WRITE);
}

void Server(event_base* base)
{
	//监听端口
	sockaddr_in addr;
	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(PORT);
	evconnlistener* listener = evconnlistener_new_bind(base,
		listen_cb_server, //回调函数
		base,	//回调函数参数
		LEV_OPT_REUSEABLE | LEV_OPT_CLOSE_ON_FREE, //地址重用，listen关闭同时清理socket
		10, //连接的最大队列大小，对于listen函数
		(sockaddr *)&addr, sizeof(addr)	//绑定的地址和端口
	);

}
