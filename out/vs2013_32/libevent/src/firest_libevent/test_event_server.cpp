

#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <event2/event.h>
#include <thread>
#include <errno.h>
#include <string.h>
#include <locale.h>

using namespace std;
#define PORT 5001

//正常断开连接和超时也会进入。
void Client_cb(evutil_socket_t s, short w, void* arg)
{
	//水平触发时，只有有数据没处理，就会一直触发
	//边缘触发，如果数据未收完成不会再触发，所以再recv中要读取完成。
	cout << "." << flush; return;

	event* ev = (event*)arg;
	//判断超时事件
	if (w & EV_TIMEOUT)
	{
		cout << "time out" << endl;
		event_free(ev);
		evutil_closesocket(s);
		return;
	}
	char buf[1024] = { 0 };
	int len = ::recv(s, buf, sizeof(buf) - 1, 0);
	if (len > 0)
	{
		cout << "recv:" << buf << endl;
		::send(s, "ok", 2, 0);
	}
	else
	{
		cout << "." << flush;//不处理的话，会一直触发对于windows和水平出法。
		//需要清理event;
		event_free(ev);
		evutil_closesocket(s);
	}
}

void Listen_cb(evutil_socket_t s, short w, void* arg)
{
	cout << "listen_cb:" << endl;//不处理的话，会一直触发对于windows和水平出法。
	sockaddr_in sin;
	socklen_t size = sizeof(sin);
	//读取连接信息
	evutil_socket_t client = ::accept(s, (sockaddr*)&sin, &size);
	char ip[16] = { 0 };
	evutil_inet_ntop(AF_INET, &sin, ip, sizeof(ip)-1);
	cout << "client ip is " << ip << endl;

	//客户端数据读取事件
	event_base* base = (event_base*)arg;
	//水平触发
	//event *ev = event_new(base, client, EV_READ | EV_PERSIST, Client_cb, event_self_cbarg());

	//边缘触发,window无效
	event *ev = event_new(base, client, EV_READ | EV_PERSIST | EV_ET, Client_cb, event_self_cbarg());

	timeval t = { 10, 0 };
	event_add(ev, &t);
}

#if 0 
int main(int argc, char* argv[])
{
#ifdef _WIN32
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);
#else
	//忽略管道信号，发送数据给已关闭的socket。错误忽略掉，防止程序dump掉
	if (signed(SIGPIPE, SIG_IGN) == SIG_ERR)
		return 1;
#endif

	event_base *base = event_base_new();
	//event server
	cout << "test event server " << endl;
	evutil_socket_t sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock <= 0)
	{
		cout << "socket error" << strerror(errno) << endl;
		return -1;
	}

	//地址复用和非阻塞
	evutil_make_socket_nonblocking(sock);
	evutil_make_listen_socket_reuseable(sock);

	sockaddr_in sin;
	memset(&sin, 0, sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_port =htons(PORT);
	int re = ::bind(sock, (sockaddr*)&sin, sizeof(sin));
	if (re != 0)
	{
		cerr << "bind error:" << strerror(errno) << endl;
		return -1;
	}
	listen(sock, 10);
	//linux中默认是水平触发。
	event *ev = event_new(base, sock, EV_READ | EV_PERSIST, Listen_cb, base);
	event_add(ev, NULL);

	event_base_dispatch(base);//运行循环，阻塞函数
	event_free(ev);
	evutil_closesocket(sock);
	event_base_free(base);
	return 0;
}
#endif

//读linux系统登陆文件/var/log/auth.log

//makefile:
//test_event_server:test_event_server.cpp
//g++ $^ -o $@ -levent

//make:

//test:telnet 127.0.0.1 5001
