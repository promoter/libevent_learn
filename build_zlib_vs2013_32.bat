@echo "start build zlib"
set VS="C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\vcvarsall.bat"
set OUT="E:\libevent_learn\out\vs2013_32\zlib"
call %VS%
cd E:\libevent_learn\environment\zlib-1.2.11
nmake /f win32\Makefile.msc clean
nmake /f win32\Makefile.msc 
md %OUT%\lib
md %OUT%\bin
md %OUT%\include
copy /Y *.lib %OUT%\lib
copy /Y *.exe %OUT%\bin
copy /Y *.dll %OUT%\bin
copy /Y *.h %OUT%\include
@echo "build zlib end"
pause