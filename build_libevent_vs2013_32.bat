@echo "start build libevent"
set VS="C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\vcvarsall.bat"
set OUT="E:\libevent_learn\out\vs2013_32\libevent"
call %VS%
E:
cd E:\libevent_learn\environment\libevent-master
nmake /f Makefile.nmake clean
nmake /f Makefile.nmake OPENSSL_DIR="E:\libevent_learn\out\vs2013_32\openssl"
md %OUT%\lib
md %OUT%\bin
md %OUT%\include
copy /Y *.lib %OUT%\lib
copy /Y test\*.exe %OUT%\bin
copy /Y test\*.dll %OUT%\bin
xcopy /S/Y include %OUT%\include\
xcopy /S/Y WIN32-Code\nmake %OUT%\include\
@echo "build libevent end"
pause